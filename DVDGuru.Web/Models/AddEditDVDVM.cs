﻿using DVDGuru.Data;
using DVDGuru.Data.Models;
using DVDGuru.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DVDGuru.Web.Models
{
    public class AddEditDVDVM
    {
        public DVD DVD { get; set; }
        public List<SelectListItem> MPAARatingList { get; set; }
        public List<SelectListItem> UserRatingList { get; set; }

        public AddEditDVDVM()
        {
           // DVDRepository _repo = new DVDRepository();
            DVD = new DVD();
            MPAARatingList = new List<SelectListItem>();
            UserRatingList = new List<SelectListItem>(); 
        }
    }
}
