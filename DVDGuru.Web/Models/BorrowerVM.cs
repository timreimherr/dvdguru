﻿using DVDGuru.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DVDGuru.Web.Models
{
    public class BorrowerVM
    {
        public DVD DVD { get; set; }
        public Borrower Borrower { get; set; }
        public LoanTime LoanTime { get; set; }

        public BorrowerVM()
        {
            DVD = new DVD();
            Borrower = new Borrower();
            LoanTime = new LoanTime();
        }
    }
}