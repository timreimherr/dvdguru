﻿using DVDGuru.Data;
using DVDGuru.Data.Models;
using DVDGuru.Web.Models;
using System;
using System.Web.Mvc;


namespace DVDGuru.Web.Controllers
{
    public class DVDController : Controller
    {
        //TODO: rename repos
        private IDVDRepository _repo;
        private IBorrowerRepository _repoBorrower;
        private IMPAARatingRepository _repoMR;
        private IUserRatingRepository _repoUR;
        private ILoanTimeRepository _repoLoanTime;

        public DVDController(IDVDRepository dvdRepository, IBorrowerRepository borrowerRepository, IMPAARatingRepository mpaaRepository, IUserRatingRepository userRatingRepository, ILoanTimeRepository loanTimeRepository)
        {
            _repo = dvdRepository;
            _repoBorrower = borrowerRepository;
            _repoMR = mpaaRepository;
            _repoUR = userRatingRepository;
            _repoLoanTime = loanTimeRepository;
        }

        [HttpPost]
        public ActionResult Search(string title)
        {
            //TODO: check that 'title' isn't null
            var model = _repo.GetSearchedTitles(title);
            return View(model);
        }
        // GET DVD/Details
        public ActionResult Details(int id)
        {
            //TODO: check that 'id' isn't null
            var model = _repo.GetSingleDvd(id);
            return View(model);
        }

        // GET: DVD/Create
        public ActionResult Add()
        {
            //TODO: refactor GetNewDVDVMOrFillLists() to create a new filled VM or take in a parameter
            var model = GetNewDVDVMOrFillLists();
            model.DVD.Available = true;
            return View(model);
        }

        // POST: DVD/Create
        [HttpPost]
        public ActionResult Add(AddEditDVDVM newDVDAddEditViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _repo.Insert(newDVDAddEditViewModel.DVD);
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception)
                {
                    //TODO: Show an error view
                    return View(newDVDAddEditViewModel);
                }
            }
            else
            {
                //TODO: research binding issue
                GetNewDVDVMOrFillLists(newDVDAddEditViewModel);
                return View(newDVDAddEditViewModel);
            }
        }



        private AddEditDVDVM GetNewDVDVMOrFillLists()
        {
            var model = new AddEditDVDVM();
            var mpaaRatings = _repoMR.GetAllMPAARatings();
            var userRatings = _repoUR.GetAllUserRatings();
            foreach (var rating in mpaaRatings)
            {
                SelectListItem newMPAARating = new SelectListItem()
                {
                    Text = rating.MPAARatings,
                    Value = rating.MPAARatingID.ToString()
                };

                model.MPAARatingList.Add(newMPAARating);
            }
            foreach (var uRating in userRatings)
            {
                SelectListItem newUserRatings = new SelectListItem()
                {
                    Text = uRating.Stars.ToString(),
                    Value = uRating.UserRatingID.ToString()
                };

                model.UserRatingList.Add(newUserRatings);
            }
            return model;
        }

        private AddEditDVDVM GetNewDVDVMOrFillLists(AddEditDVDVM emptyAddEditDVDVM)
        {

            var mpaaRatings = _repoMR.GetAllMPAARatings();
            var userRatings = _repoUR.GetAllUserRatings();
            foreach (var rating in mpaaRatings)
            {
                SelectListItem newMPAARating = new SelectListItem()
                {
                    Text = rating.MPAARatings,
                    Value = rating.MPAARatingID.ToString()
                };

                emptyAddEditDVDVM.MPAARatingList.Add(newMPAARating);
            }
            foreach (var uRating in userRatings)
            {
                SelectListItem newUserRatings = new SelectListItem()
                {
                    Text = uRating.Stars.ToString(),
                    Value = uRating.UserRatingID.ToString()
                };

                emptyAddEditDVDVM.UserRatingList.Add(newUserRatings);
            }
            return emptyAddEditDVDVM;
        }


        // GET: DVD/Edit/5
        public ActionResult Lend(int id)
        {
            var model = new BorrowerVM();
            model.LoanTime.LoanDate = DateTime.Now;
            model.DVD = _repo.GetSingleDvd(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Lend(BorrowerVM newBorrower)
        {
            try
            {

                _repoBorrower.Insert(newBorrower.Borrower);
                //TODO: use var dvd
                newBorrower.DVD = _repo.GetSingleDvd(newBorrower.DVD.DVDID);
                newBorrower.DVD.Available = false;
                _repo.Edit(newBorrower.DVD);
                //TODO: use var loanTime
                newBorrower.LoanTime.DVDID = newBorrower.DVD.DVDID;
                newBorrower.LoanTime.BorrowerID = newBorrower.Borrower.BorrowerID;
                _repoLoanTime.Insert(newBorrower.LoanTime);

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View(newBorrower);
            }
        }
        [HttpPost]
        public ActionResult Received(int id)
        {
            try
            {
                var dvd = _repo.GetSingleDvd(id);
                dvd.Available = true;
                _repo.Edit(dvd);
                var loanTime = _repoLoanTime.GetLoanTimeRecord(id);
                //TODO: rename all suffix all DateTime properites with UTC (ie. DateTime.UtcNow)
                //TODO: use DateTime.UtcNow instead of DateTime.Now
                loanTime.ReturnedDate = DateTime.Now;
                _repoLoanTime.ReturnDVD(loanTime);
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                _repo.Delete(id);
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

    }
}
