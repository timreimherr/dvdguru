﻿using System.Web.Mvc;
using DVDGuru.Data;

namespace DVDGuru.Web.Controllers
{
    public class HomeController : Controller
    {
        private IDVDRepository _repo;

        public HomeController(IDVDRepository dvdRepository)
        {
            _repo = dvdRepository;
        }

        public ActionResult Index()
        {
            return View(_repo.GetAllDVDsList());
        }
    }
}