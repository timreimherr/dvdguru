﻿$(document).ready(function () {
    $('#addEditForm').validate({
        rules: {
            Title: {
                required: true
            },
            ReleaseDate: {
                required: true,
            },
            MPAARatingID: {
                required: true
            },
            UserRatingID: {
                required: true,
            },
        },
        messages: {
            Name: "Enter a DVD title",
            ReleaseDate: "Enter the release date",
            MPAARatingID: "Set the MPAA Rating",
            UserRatingID: "Set a user rating"
        }
    });
});