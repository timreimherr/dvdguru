﻿using DVDGuru.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDGuru.Data.Models;

namespace DVDGuru.Web.Tests
{
    public class MockLoanTimeRepo : ILoanTimeRepository
    {
        public LoanTime GetLoanTimeRecord(int id)
        {
            throw new NotImplementedException();
        }

        public void Insert(LoanTime newLoanTime)
        {
            throw new NotImplementedException();
        }

        public void ReturnDVD(LoanTime receivedDVD)
        {
            throw new NotImplementedException();
        }
    }
}
