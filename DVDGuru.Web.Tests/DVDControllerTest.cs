﻿using System;
using DVDGuru.Web.Controllers;
using System.Web.Mvc;
using NUnit.Framework;
using DVDGuru.Data;

namespace DVDGuru.Web.Tests
{
    [TestFixture]
    public class DVDControllerTest
    {
        private IDVDRepository _repo;
        private IBorrowerRepository _repoBorrower;
        private ILoanTimeRepository _repoLoanTime;
        private IMPAARatingRepository _repoMPAARating;
        private IUserRatingRepository _repoUserRating;

        public DVDControllerTest()
        {
            
        }
        [SetUp]
        public void SetUpTest()
        {
            _repo = new MockDVDRepo();
            _repoBorrower = new MockBorrowerRepo();
            _repoLoanTime = new MockLoanTimeRepo();
            _repoMPAARating = new MockMPAARepo();
            _repoUserRating = new MockUserRatingRepository();
        }

        [TearDown]
        public void TearDownTest()
        {
            _repo = null;
            _repoBorrower = null;
            _repoLoanTime = null;
            _repoMPAARating = null;
            _repoUserRating = null;
        }
        [Test]
        public void EnsureDetailsIsNotNull()
        {
            // Arrange
            DVDController controller = new DVDController(_repo, _repoBorrower, _repoMPAARating, _repoUserRating, _repoLoanTime);

            // Act
            ViewResult result = controller.Details(10) as ViewResult;

            // Assert
            Assert.IsNotNull(result.Model);
        }

        [Test]
        public void EnsureSearchIsNotNull()
        {
            DVDController controller = new DVDController(_repo, _repoBorrower, _repoMPAARating, _repoUserRating, _repoLoanTime);

            ViewResult result = controller.Search("Tylers") as ViewResult;

            Assert.IsNotNull(result.Model);
        }

        [Test]
        public void EnsuresEditGetsADVD()
        {
            DVDController controller = new DVDController(_repo, _repoBorrower, _repoMPAARating, _repoUserRating, _repoLoanTime);

            ViewResult result = controller.Edit(7) as ViewResult;

            Assert.IsNotNull(result.Model);

        }

    }
    
}
