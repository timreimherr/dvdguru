﻿using DVDGuru.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDGuru.Data.Models;

namespace DVDGuru.Web.Tests
{
    public class MockDVDRepo : IDVDRepository
    {
        List<DVD> _testRepo;
        public MockDVDRepo()
        {
            _testRepo = new List<DVD>()
            {
                new DVD
                {
                    DVDID = 10,
                    Title = "Tyler's New Movie"
                },
                new DVD
                {
                    DVDID = 7,
                    Title = "Tim's Movie"
                }
            };
        }

        public void Delete(int id)
        {
            _testRepo.RemoveAll(m => m.DVDID == id);
        }

        public List<DVD> GetAllDVDsList()
        {
            return _testRepo;
        }

        public DVD GetSingleDvd(int id)
        {
            return _testRepo.SingleOrDefault(m => m.DVDID == id);
        }

        public List<DVD> GetSearchedTitles(string title)
        {
            return _testRepo.Where(m => m.Title.Contains(title)).ToList();
        }

        public void Insert(DVD newDVD)
        {
            _testRepo.Add(newDVD);
        }

        public void Edit(DVD dvd)
        {
            _testRepo.RemoveAll(m => m.DVDID == dvd.DVDID);
            _testRepo.Add(dvd);
        }
    }
}
