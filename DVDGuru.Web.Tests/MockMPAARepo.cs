﻿using DVDGuru.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDGuru.Data.Models;

namespace DVDGuru.Web.Tests
{
    public class MockMPAARepo : IMPAARatingRepository
    {
        List<MPAARating> _mockMpaaRepo;
        public MockMPAARepo()
        {
            _mockMpaaRepo = new List<MPAARating>()
            {
                new MPAARating
                {
                    MPAARatingID = 1,
                    MPAARatings = "G"
                },
                new MPAARating
                {
                    MPAARatingID = 2,
                    MPAARatings = "PG"
                },
                new MPAARating
                {
                    MPAARatingID = 3,
                    MPAARatings = "PG-13"
                },
                new MPAARating
                {
                    MPAARatingID = 4,
                    MPAARatings = "R"
                },
                new MPAARating
                {
                    MPAARatingID = 5,
                    MPAARatings = "NC-17"
                }
            };
        }

        public List<MPAARating> GetAllMPAARatings()
        {
            return _mockMpaaRepo;
        }

        public MPAARating GetMPAARating(int id)
        {
            throw new NotImplementedException();
        }
    }
}
