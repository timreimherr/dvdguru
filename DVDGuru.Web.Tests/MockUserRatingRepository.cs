﻿using DVDGuru.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDGuru.Data.Models;

namespace DVDGuru.Web.Tests
{
    public class MockUserRatingRepository : IUserRatingRepository
    {
        List<UserRating> _mockUserRatingRepo;
        public MockUserRatingRepository()
        {
            _mockUserRatingRepo = new List<UserRating>
            {
                new UserRating
                {
                    Description = "Very Bad",
                    Stars = 1
                },
               new UserRating
                {
                    Description = "Bad",
                    Stars = 2
                },
               new UserRating
                {
                    Description = "Meh",
                    Stars = 3
                },
               new UserRating
                {
                    Description = "Good",
                    Stars = 4
                },
                new UserRating
                {
                    Description = "very Good",
                    Stars = 5
                }
            };
        }

        public List<UserRating> GetAllUserRatings()
        {
            return _mockUserRatingRepo;
        }
    }
}
