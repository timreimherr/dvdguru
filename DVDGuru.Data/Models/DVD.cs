﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data.Models
{
    public class DVD
    {
        [Key]
        public int DVDID { get; set; }
        [Required(ErrorMessage ="Please enter a title")]
        public string Title { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage ="Please enter a date")]
        public DateTime ReleaseDate { get; set; }
        public string DirectorNames { get; set; }
        public string StudioName { get; set; }
        public string ActorNames { get; set; }
        public string UserNote { get; set; }
        [Required(ErrorMessage ="Please enter a MPAA Rating")]
        public int MPAARatingID { get; set; }
        [Required(ErrorMessage ="Please enter a User Rating")]
        public int UserRatingID { get; set; }
        public bool Available { get; set; }
        public MPAARating MPAARating { get; set; }
        public UserRating UserRating { get; set; }
    }
}
