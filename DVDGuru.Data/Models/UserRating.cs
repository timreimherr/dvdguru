﻿using System.ComponentModel.DataAnnotations;

namespace DVDGuru.Data.Models
{
    public class UserRating
    {
        [Key]
        public int UserRatingID { get; set; }
        public double Stars { get; set; }
        public string Description { get; set; }
    }
}