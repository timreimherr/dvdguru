﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data.Models
{
    public class Borrower
    {
        [Key]
        public int BorrowerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
