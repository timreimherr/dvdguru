﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data.Models
{
    public class DVDContext : DbContext
    {
        public DbSet<DVD> DVDs { get; set; }
        public DbSet<MPAARating> MPAARatings { get; set; }
        public DbSet<UserRating> UserRatings { get; set; }
        public DbSet<Borrower> Borrowers { get; set; }
        public DbSet<LoanTime> LoanTimes { get; set; }
    }
}
