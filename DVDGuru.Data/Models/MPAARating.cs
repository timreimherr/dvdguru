﻿using System.ComponentModel.DataAnnotations;

namespace DVDGuru.Data.Models
{
    public class MPAARating
    {
        [Key]
        public int MPAARatingID { get; set; }
        public string MPAARatings{ get; set; }
    }
}