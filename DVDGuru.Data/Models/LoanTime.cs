﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data.Models
{
    public class LoanTime
    {
        public int LoanTimeID { get; set; }
        public int BorrowerID { get; set; }
        public int DVDID { get; set; }
        [DataType(DataType.Date)]
        public DateTime LoanDate { get; set; }
        public DateTime? ReturnedDate { get; set; }
        public Borrower Borrower { get; set; }
        public DVD DVD { get; set; }
    }
}
