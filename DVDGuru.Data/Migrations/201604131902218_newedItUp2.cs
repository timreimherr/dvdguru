namespace DVDGuru.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newedItUp2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DVDs", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DVDs", "Title", c => c.String());
        }
    }
}
