namespace DVDGuru.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDVDs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Borrowers",
                c => new
                    {
                        BorrowerID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.BorrowerID);
            
            CreateTable(
                "dbo.DVDs",
                c => new
                    {
                        DVDID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ReleaseDate = c.DateTime(nullable: false),
                        DirectorNames = c.String(),
                        StudioName = c.String(),
                        ActorNames = c.String(),
                        UserNote = c.String(),
                        MPAARatingID = c.Int(nullable: false),
                        UserRatingID = c.Int(nullable: false),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DVDID)
                .ForeignKey("dbo.MPAARatings", t => t.MPAARatingID, cascadeDelete: true)
                .ForeignKey("dbo.UserRatings", t => t.UserRatingID, cascadeDelete: true)
                .Index(t => t.MPAARatingID)
                .Index(t => t.UserRatingID);
            
            CreateTable(
                "dbo.MPAARatings",
                c => new
                    {
                        MPAARatingID = c.Int(nullable: false, identity: true),
                        MPAARatings = c.String(),
                    })
                .PrimaryKey(t => t.MPAARatingID);
            
            CreateTable(
                "dbo.UserRatings",
                c => new
                    {
                        UserRatingID = c.Int(nullable: false, identity: true),
                        Stars = c.Double(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.UserRatingID);
            
            CreateTable(
                "dbo.LoanTimes",
                c => new
                    {
                        LoanTimeID = c.Int(nullable: false, identity: true),
                        BorrowerID = c.Int(nullable: false),
                        DVDID = c.Int(nullable: false),
                        LoanDate = c.DateTime(nullable: false),
                        ReturnedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.LoanTimeID)
                .ForeignKey("dbo.Borrowers", t => t.BorrowerID, cascadeDelete: true)
                .ForeignKey("dbo.DVDs", t => t.DVDID, cascadeDelete: true)
                .Index(t => t.BorrowerID)
                .Index(t => t.DVDID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanTimes", "DVDID", "dbo.DVDs");
            DropForeignKey("dbo.LoanTimes", "BorrowerID", "dbo.Borrowers");
            DropForeignKey("dbo.DVDs", "UserRatingID", "dbo.UserRatings");
            DropForeignKey("dbo.DVDs", "MPAARatingID", "dbo.MPAARatings");
            DropIndex("dbo.LoanTimes", new[] { "DVDID" });
            DropIndex("dbo.LoanTimes", new[] { "BorrowerID" });
            DropIndex("dbo.DVDs", new[] { "UserRatingID" });
            DropIndex("dbo.DVDs", new[] { "MPAARatingID" });
            DropTable("dbo.LoanTimes");
            DropTable("dbo.UserRatings");
            DropTable("dbo.MPAARatings");
            DropTable("dbo.DVDs");
            DropTable("dbo.Borrowers");
        }
    }
}
