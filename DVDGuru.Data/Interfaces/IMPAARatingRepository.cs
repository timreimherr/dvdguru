﻿using DVDGuru.Data.Models;
using System.Collections.Generic;

namespace DVDGuru.Data
{
    public interface IMPAARatingRepository
    {
        List<MPAARating> GetAllMPAARatings();
        MPAARating GetMPAARating(int id);
    }
}