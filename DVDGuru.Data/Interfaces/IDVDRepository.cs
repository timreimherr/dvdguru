﻿using System.Collections.Generic;
using DVDGuru.Data.Models;

namespace DVDGuru.Data
{
    public interface IDVDRepository
    {
        List<DVD> GetAllDVDsList(); 
        DVD GetSingleDvd(int id);
        List<DVD> GetSearchedTitles(string title);
        void Insert(DVD newDVD);
        void Delete(int id);
        void Edit(DVD dvd);
    }
}