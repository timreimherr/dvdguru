﻿using DVDGuru.Data.Models;
using System.Collections.Generic;

namespace DVDGuru.Data
{
    public interface IUserRatingRepository
    {
        List<UserRating> GetAllUserRatings();
    }
}