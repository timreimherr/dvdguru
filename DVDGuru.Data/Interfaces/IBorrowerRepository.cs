﻿using DVDGuru.Data.Models;

namespace DVDGuru.Data
{
    public interface IBorrowerRepository
    {
        Borrower GetBorrower(int id);
        void Insert(Borrower newBorrower);
    }
}