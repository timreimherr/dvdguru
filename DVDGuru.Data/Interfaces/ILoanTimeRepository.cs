﻿using DVDGuru.Data.Models;

namespace DVDGuru.Data
{
    public interface ILoanTimeRepository
    {
        LoanTime GetLoanTimeRecord(int id);
        void Insert(LoanTime newLoanTime);
        void ReturnDVD(LoanTime receivedDVD);
    }
}