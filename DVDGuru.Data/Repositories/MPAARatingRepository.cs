﻿using DVDGuru.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data
{
    public class MPAARatingRepository : IMPAARatingRepository
    {
        public List<MPAARating> GetAllMPAARatings()
        {
            using (var database = new DVDContext())
            {
                return database.MPAARatings.ToList();
            }
        }
        public MPAARating GetMPAARating(int id)
        {
            using (var database = new DVDContext())
            {
                return database.MPAARatings.SingleOrDefault(m => m.MPAARatingID == id);
            }
        }
    }
}
