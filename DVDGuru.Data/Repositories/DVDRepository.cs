﻿using DVDGuru.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace DVDGuru.Data.Repositories
{
    public class DVDRepository : IDVDRepository
    {
        public List<DVD> GetAllDVDsList()
        {
            using (DVDContext database = new DVDContext())
            {
                return database.DVDs.Include("MPAARating").Include("UserRating").ToList();
            }
        }

        public void Insert(DVD newDVD)
        {
            using (var database = new DVDContext())
            {
                database.DVDs.Add(newDVD);
                database.SaveChanges();
            }
        }


        public void Edit(DVD editedDVD)
        {
            using (var database = new DVDContext())
            {
                var originalRecord = database.DVDs.Find(editedDVD.DVDID);
                if (originalRecord != null)
                {
                    database.Entry(originalRecord).CurrentValues.SetValues(editedDVD);
                    database.SaveChanges();
                }
            }
        }

        public DVD GetSingleDvd(int id)
        {
        
            using(var database = new DVDContext())
            {

                var dvd = database.DVDs.Include("MPAARating").Include("UserRating").SingleOrDefault(m => m.DVDID == id);
                return dvd;

            }            
        }
        
        public List<DVD> GetSearchedTitles(string title)
        {
            //TODO: add Try-Catch for bad string or number with a block of code for that catch
            using (var database = new DVDContext())
            {
                return database.DVDs.Include("MPAARating").Include("UserRating").Where(m=>m.Title.Contains(title)).ToList();
            }
        }

        public void Delete (int id)
        {
            using (var database = new DVDContext())
            {
                var dvd = database.DVDs.SingleOrDefault(d => d.DVDID == id);
                database.DVDs.Remove(dvd);
                database.SaveChanges();
            }
        }

    }
}
