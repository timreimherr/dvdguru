﻿using DVDGuru.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data.Repositories
{
    public class LoanTimeRepository : ILoanTimeRepository
    {
        public void Insert (LoanTime newLoanTime)
        {
            using (var database = new DVDContext())
            {
                database.LoanTimes.Add(newLoanTime);
                database.SaveChanges();
            }
        }
        public void ReturnDVD(LoanTime receivedDVD)
        {
            using (var database = new DVDContext())
            {
                var originalRecord = database.LoanTimes.Find(receivedDVD.DVDID);
                if (originalRecord != null)
                {
                    database.Entry(originalRecord).CurrentValues.SetValues(receivedDVD);
                    database.SaveChanges();
                }
            }
        }
        public LoanTime GetLoanTimeRecord(int id)
        {
            using (var database = new DVDContext())
            {

                var LoanTimeRecord = database.LoanTimes.Include("DVD").Include("Borrower").FirstOrDefault(m => m.DVDID == id);
                return LoanTimeRecord;

            }
        }
    }
}
