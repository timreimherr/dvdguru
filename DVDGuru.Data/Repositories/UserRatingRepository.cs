﻿using DVDGuru.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data
{
    public class UserRatingRepository : IUserRatingRepository
    {
        public List<UserRating> GetAllUserRatings()
        {
            using (var database = new DVDContext())
            {
                return database.UserRatings.ToList();
            }
        }
    }
}
