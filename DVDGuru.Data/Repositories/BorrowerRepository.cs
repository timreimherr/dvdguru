﻿using DVDGuru.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDGuru.Data
{
    public class BorrowerRepository : IBorrowerRepository
    {
        public void Insert(Borrower newBorrower)
        {
            //TODO check to see if newBorrower is null or not
            using (var database = new DVDContext())
            {
                database.Borrowers.Add(newBorrower);
                database.SaveChanges();
            }
        }
        public Borrower GetBorrower(int id)
        {
            using (var database = new DVDContext())
            {
                var borrower = database.Borrowers.FirstOrDefault(m => m.BorrowerID == id);
                return borrower;
            }
        }


    }
}
